#!/bin/bash
. venv/bin/activate
python -m flake8 . --extend-exclude=dist,build,venv --max-line-length=120 --show-source --statistics
