#!/bin/bash
. venv/bin/activate
PYTHONPATH=src python -m pytest -v tests/* -p no:warnings
